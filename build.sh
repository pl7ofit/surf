#!/bin/bash

set -eu

name=surf
version="2.1-git"
repo="git://git.suckless.org/dwm"


docker build -f Dockerfile . -t surf:latest \
       --build-arg "version=${version}" \
       --build-arg "name=${name}"
original_hash="$(docker run  surf:latest sh -c "sha256sum surf-$version.deb")"
docker run surf:latest  sh -c "cat surf-$version.deb" > surf-$version.deb
copy_hash="$(sha256sum surf-$version.deb)"
echo $original_hash
echo $copy_hash

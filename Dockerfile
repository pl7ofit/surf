from ubuntu:20.04 as Ubuntu_deps
WORKDIR /src

# setup apt envs
ENV TZ=Europe/Kaliningrad
ENV DEBIAN_FRONTEND=noninteractive
ENV ARGS "-o Acquire::GzipIndexes=false -o APT::Sandbox::User=root"
USER root

# install deps
RUN chown -Rv _apt:root /var/cache/apt/archives/partial/
RUN chmod -Rv 700 /var/cache/apt/archives/partial/
RUN sed -i 's|^# deb-src|deb-src|g' /etc/apt/sources.list
RUN apt-get $ARGS update
RUN apt-get $ARGS install -y eatmydata dpkg-dev
RUN eatmydata -- apt-get $ARGS install -y --no-install-recommends \
                                         git \
                                         libglib2.0-dev \
                                         libwebkit2gtk-4.0-dev \
                                         libgcr-3-dev \
                                         make gcc
# setup args
ARG version
ARG name
#ARG mouse_id
# copy 
COPY . .

#RUN sed -i "s|#define mouse_id.*|#define mouse_id \"${mouse_id}\"|g" config.def.h


#from ubuntu:20.04 as Builder
#COPY --from=Deps / /
# apply patchs
#RUN for p in patchs/*.diff; \
#    do \
#      echo "PATCH: "${p}; \
#      patch --verbose dwm.c "${p}"; \
#      if [ "$?" -ne "0" ]; \
#      then \
#         ls -1 | grep ".rej"; \
#         cat *.rej; \
#         cat dwm.c; \
#         exit 1; \
#      fi \
#    done
#RUN patch -p1 < patchs/000-dwm-systray-6.2.diff



# compile
WORKDIR /src
RUN DESTDIR=/build/${name}-${version} make install
# copy deb dir
WORKDIR /build

# copy desktop file
#COPY ${name}.desktop /build/${name}-${version}/usr/share/xsessions/${name}.desktop

# copy wrapper script
#COPY dwm-wrapper.sh /build/${name}-${version}/usr/bin/

# copy bar status script
#COPY dwm-bar.sh /build/${name}-${version}/usr/bin/

# copy bg script
#COPY dwm-bg.sh /build/${name}-${version}/usr/bin/

# copy icon 
#COPY dwm.png /build/${name}-${version}/usr/share/pixmaps/

# copy compton config
#COPY compton.conf /build/${name}-${version}/etc/


FROM ubuntu:20.04 as Ubuntu_build
# setup args
ARG version
ARG name
WORKDIR /build
COPY --from=Ubuntu_deps /build /build
COPY DEBIAN ${name}-${version}/DEBIAN
RUN sed -i "s|^Version.*|Version: ${version}|g" ${name}-${version}/DEBIAN/control

# build deb
RUN find . -name '*'
RUN dpkg --build ${name}-${version}
#RUN apt update
#RUN apt install --no-install-recommends -y ./${name}-${version}.deb



#from voidlinux/voidlinux:latest as Voidlinux_build
# setup args
#ARG version
#ARG name
#WORKDIR /build
#COPY --from=Ubuntu_deps /build /build
#RUN xbps-create -A amd64 -n dwm-6.2_0 -s "dwm" /build/*
#RUN xbps-install -Syu xtools
#RUN xdowngrade ./*.xbps
#RUN xbps-install ./*.xbps
